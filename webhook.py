#!/usr/bin/env python

import os
import bottle
import pprint
import logging
import subprocess
from bottle import route, Bottle, run, request

isWindows = True
# logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# create file handler
logfile = '/var/log/webhook.log' if not isWindows else 'd:/tmp/webhook.log'
handler = logging.FileHandler(logfile)
handler.setLevel(logging.ERROR) if isWindows else None

# create logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

# this service reads from environment
# the following vars:
# LISTEN_ADDRESS, LISTEN_PORT, REPO_FULL_NAME,
# REPO_BRANCH & REPO_PATH
'''
LISTEN_ADDRESS = os.environ['LISTEN_ADDRESS'] or 'web.ectivisecloud.com'
LISTEN_PORT = os.environ['LISTEN_PORT']
REPO_FULL_NAME = os.environ['REPO_FULL_NAME']
REPO_PATH = os.environ['REPO_PATH']
CMD = ['cd', REPO_PATH, '&&', 'git', 'pull', '--rebase']
'''
LISTEN_ADDRESS = 'web.ectivisecloud.com' if not isWindows else 'localhost'
LISTEN_PORT = 8000

app = bottle.app()


def LOGER(msg1, msg):
    logger.info(msg1, msg)


@bottle.post('/webhook')
def hook():
    pp = pprint.PrettyPrinter(indent=4)
    payload = request.json
    logger.debug(pp.pprint(payload))
    name = payload['repository']['full_name'] # not sure that that's the right key
    LOGER('Receive request for %s', name)
    # if name == REPO_FULL_NAME:
    #     logger.info('Execute %s', CMD)
    #     subprocess.Popen(CMD)
    # else:
    #     logger.error('Request is for different repo than %s', REPO_FULL_NAME)


@route('/hello')
def hello():
    return "Hello World!"


@bottle.post('/login')  # or @route('/login', method='POST')
def do_login():
    username = request.forms.get('username')
    password = request.forms.get('password')
    if check_login(username, password):
        return "<p>Your login information was correct.</p>"
    else:
        return "<p>Login failed.</p>"


def check_login(name, pwd):
    return name == 'liqin' and pwd == 'nothing'


def main():
    bottle.run(app=app, host=LISTEN_ADDRESS, port=LISTEN_PORT, quiet=False, debug=True)


if __name__ == '__main__':
    main()
